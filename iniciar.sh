#!/bin/bash

dir="/var/www/app/"
Email=rtz645@gmail.com

echo -e "Domínio:  \c "
read  dominioServidor


function function_Instalar {
    function_Lemp
    SSL
}



function function_Lemp {
sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y dist-upgrade


### Install mysql server
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
sudo apt-get install -y mysql-server

sudo cp /etc/mysql/my.cnf /etc/mysql/my.cnf.default

sudo sed -ri 's/key_buffer(\s.*)/#key_buffer\1\nkey_buffer_size\1/g' /etc/mysql/my.cnf

sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf



sudo apt-get -y install nginx
sudo apt-get -y install php7.0-cli php7.0-fpm php7.0-mysql php7.0-curl php7.0-gd php-memcached php7.0-dev php7.0-mcrypt php7.0-sqlite3 php7.0-mbstring
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/7.0/fpm/php.ini
sudo systemctl restart php7.0-fpm


sudo mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default_config
cat arquivos/default > /etc/nginx/sites-available/default

    ## PHP SERVER
    sed -i "s|{dominio}|$dominioServidor|g" /etc/nginx/sites-available/default
    sed -i "s|{dire}|$dir|g" /etc/nginx/sites-available/default


    sudo service php7.0-fpm restart && service nginx restart
    phpenmod mcrypt
    sudo service php7.0-fpm restart
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
    clear
    echo 'L3MP ~ Sucesso!!'

}

function SSL {

# Update
sudo apt-get -qq update

# Install git
sudo apt-get -y install git

# Stop nginx
sudo service nginx stop

# Clone Let's Encrypt
cd ~
mkdir letsencrypt
sudo git clone https://github.com/letsencrypt/letsencrypt letsencrypt/
cd letsencrypt
sudo ./letsencrypt-auto certonly --standalone --email $Email -d $dominioServidor -d www.$dominioServidor --agree-tos --text

# Start nginx back up
service nginx start
cd ~
}



function_Instalar

clear
sudo service mysql restart
sudo service nginx restart
sudo service php7.0-fpm restart

echo "\e[32mSERVIDOR WEB CONFIGURADO COM SUCESSO ;)"
